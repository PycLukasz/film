package film;

// import film.read.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Controller implements ChangeListener, ActionListener {
    private GUI gui;

    public void run() {
        gui = new GUI();
        gui.createGui();
        gui.setListener(this);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        gui.correctValues((JSlider)e.getSource());
        gui.applyCorrection();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Otwórz":
                gui.openFile();
                break;
            case "Play":
                gui.playStop();
                break;
            case "Forward":
                gui.showNextFrame();
                break;
            case "Back":
                gui.showPreviousFrame();
                break;
            case "Filter":
                gui.applyFilter();
                break;
            case "Reflection":
                gui.applyReflection();
                break;
        }
    }
}
