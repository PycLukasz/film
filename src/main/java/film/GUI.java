package film;

import film.read.CineMovie;
import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.jar.JarEntry;

public class GUI extends JFrame{

    static class ImagePanel extends JPanel{
        // na podstawie: https://stackoverflow.com/questions/299495/how-to-add-an-image-to-a-jpanel
        private BufferedImage image;

        public void setImage(BufferedImage image) {
            this.image = image;
            repaint();
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        }


    }

    private CineMovie cine;

    private JFrame window;
    private final JMenuBar menuBar = new JMenuBar();
    private final JPanel toolsPane = new JPanel();
    private final JPanel imagePane = new JPanel(new BorderLayout());
    private final JPanel infoPane = new JPanel();

    private final JMenuItem open = new JMenuItem("Open", KeyEvent.VK_O);
    private final JFileChooser fileChooser = new JFileChooser();

    private JSlider gammaSlider;
    private JSlider minSlider;
    private JSlider maxSlider;

    private final String playPath = "src/main/resources/play.png";
    private final String pausePath = "src/main/resources/pause.png";
    private final String forwardPath = "src/main/resources/foward.png";
    private final String backPath = "src/main/resources/back.png";

    private final JButton back = new JButton(new ImageIcon(backPath));
    private final JButton play = new JButton(new ImageIcon(pausePath));
    private final JButton forward = new JButton(new ImageIcon(forwardPath));

    private boolean playStop = true;

    // obsługa filtrów
    private final JComboBox filterList = new JComboBox(new String[] { "None", "Sharpen", "Edge"});
    private final JComboBox reflection = new JComboBox(new String[] { "None", "Reflection X", "Reflefction Y", "Reflection XY"});

    private final ImagePanel picNew = new ImagePanel();

    private JTable infoTable;
    private JLabel resolution = new JLabel("Resolution: ");
    private JLabel currentFrame = new JLabel("Current image: ");

    Timer tm;

    public void createGui() {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // create and setup the window
        window = new JFrame("Cine Viewer");
        window.setMinimumSize(new Dimension(1300, 800));
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setLocationByPlatform(true);
        // menu setup
        window.setJMenuBar(menuBar);
        menuBar.add(open);
        fileChooser.setFileFilter(new FileNameExtensionFilter("*.cine", "cine"));

        // setup tools and image panel
        toolsPane.setLayout(new BoxLayout(toolsPane, BoxLayout.Y_AXIS));
        window.add(toolsPane, BorderLayout.WEST);
        window.add(imagePane, BorderLayout.CENTER);
        window.add(infoPane, BorderLayout.EAST);

        // tools setup
        toolsPane.setBorder(BorderFactory.createTitledBorder("Tools"));
        toolsPane.add(new JLabel("Filter"));

        // combobox do obslugi filtrow
        filterList.setSelectedIndex(0);
        filterList.setActionCommand("Filter");
        filterList.setMaximumSize(new Dimension(150, 24));
        toolsPane.add(filterList);

        toolsPane.add(new JLabel("Reflection"));
        reflection.setSelectedIndex(0);
        reflection.setActionCommand("Reflection");
        reflection.setMaximumSize(new Dimension(150, 24));
        toolsPane.add(reflection);

        // suwak dla parametru gamma
        toolsPane.add(new JLabel("Gamma"));
        gammaSlider = new JSlider(JSlider.HORIZONTAL, 1, 10000, 2000);
        toolsPane.add(gammaSlider);

        toolsPane.add(new JLabel("Min"));
        minSlider = new JSlider(JSlider.HORIZONTAL, 0, 16384, 0);
        minSlider.setMinorTickSpacing(128);
        // minSlider.setMajorTickSpacing(1024);
        minSlider.setPaintTicks(true);
        toolsPane.add(minSlider);

        toolsPane.add(new JLabel("Max"));
        maxSlider = new JSlider(JSlider.HORIZONTAL, 0, 16384, 16384);
        maxSlider.setMinorTickSpacing(128);
        // maxSlider.setMajorTickSpacing(1024);
        maxSlider.setPaintTicks(true);
        toolsPane.add(maxSlider);
        Font font = new Font(resolution.getFont().getName(), Font.PLAIN, resolution.getFont().getSize() + 1);
        resolution.setFont(font);
        toolsPane.add(resolution);
        font = new Font(resolution.getFont().getName(), Font.BOLD, resolution.getFont().getSize() + 2);
        currentFrame.setFont(font);
        toolsPane.add(currentFrame);

        // imagePane setup
        imagePane.setBorder(BorderFactory.createTitledBorder("Movie"));
        imagePane.add(picNew, BorderLayout.CENTER);

        JPanel imageButtons = new JPanel();
        imageButtons.add(back);
        imageButtons.add(play);
        imageButtons.add(forward);
        imagePane.add(imageButtons, BorderLayout.SOUTH);

        infoPane.setBorder(BorderFactory.createTitledBorder("Info"));

        infoTable = new JTable(13, 2) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 1 || column==2 ? false : false;
            }
        };
        infoTable.setPreferredSize(new Dimension(210, 210));
        infoTable.setValueAt("first movie image",0, 0);
        infoTable.setValueAt("total image count",1, 0);
        infoTable.setValueAt("first image no",2, 0);
        infoTable.setValueAt("image count",3, 0);
        infoTable.setValueAt("date",4, 0);
        infoTable.setValueAt("frame rate",5, 0);
        infoTable.setValueAt("aspect ratio",6, 0);
        infoTable.setValueAt("shutter time",7, 0);
        infoTable.setValueAt("post-trigger frames",7, 0);
        infoPane.add(infoTable, BorderLayout.CENTER);
        // show the window
        window.pack();
        window.getContentPane().setSize(WIDTH, HEIGHT);
        window.setVisible(true);

        // tworzenie timera i rozpoczecie wyswietlania filmu
        tm = new Timer(0, e -> {
            if (playStop) {
                showFrame();
                cine.nextFrame();
            }
        });
    }

    public void setListener(ActionListener controller) {
        open.setActionCommand("Otwórz");
        open.addActionListener(controller);
        play.setActionCommand("Play");
        play.addActionListener(controller);
        forward.setActionCommand("Forward");
        forward.addActionListener(controller);
        back.setActionCommand("Back");
        back.addActionListener(controller);

        filterList.addActionListener(controller);
        reflection.addActionListener(controller);
        gammaSlider.addChangeListener((ChangeListener) controller);
        minSlider.addChangeListener((ChangeListener) controller);
        maxSlider.addChangeListener((ChangeListener) controller);
    }

    public void showFrame() {
        picNew.setImage(cine.getFrame().getFrameImage());
        String[] info = cine.getInfoFromHeader();
        infoTable.setValueAt(info[0], 0, 1);
        infoTable.setValueAt(info[1], 1, 1);
        infoTable.setValueAt(info[2], 2, 1);
        infoTable.setValueAt(info[3], 3, 1);
        infoTable.setValueAt(info[4], 4, 1);
        resolution.setText("Resolution: " + info[5] + " x " + info[6]);
        currentFrame.setText("Current image: " + (cine.getImageNumber() + 1));
        infoTable.setValueAt(info[7] + " per sec", 5, 1);
        infoTable.setValueAt(info[8], 6, 1);
        infoTable.setValueAt(info[9] +" ms", 7, 1);
        infoTable.setValueAt(info[10], 7, 1);

    }

    public void applyFilter() {
        if (cine != null) {
//            final Color HIGHLIGH_COLOR = new Color(0x1DC854);
//            final Color DEFAULT_COLOR = new Color(0x62B1C3);

            // ustawienie podstawowego koloru dla kazdego z przyciskow
//            sharpen.setBackground(DEFAULT_COLOR);
//            edge.setBackground(DEFAULT_COLOR);

            // zastosowanie wybranego filtra
            switch ((String) filterList.getSelectedItem()) {
                case "Sharpen":
//                    sharpen.setBackground(HIGHLIGH_COLOR);
                    cine.setFilter(new byte[]{0, -1, 0, -1, 5, -1, 0, -1, 0});
                    break;

                case "Edge":
//                    edge.setBackground(HIGHLIGH_COLOR);
                    cine.setFilter(new byte[]{-2, -1, 0, -1, 0, 1, 0, 1, 2});
                    break;

                default:
                    cine.setFilter(null);
                    break;
            }

            showFrame();
        }


    }

    public void applyReflection() {
        if (cine != null) {
            switch ((String) reflection.getSelectedItem()) {
                case "Reflection X":
                    cine.setReflection(0);
                    break;

                case "Reflefction Y":
                    cine.setReflection(1);
                    break;

                case "Reflection XY":
                    cine.setReflection(2);
                    break;
                default:
                    cine.setReflection(-1);

            }

            showFrame();
        }
    }
    public void correctValues(JSlider slider){
         if (slider == minSlider)
            maxSlider.setMinimum(Math.max(255, minSlider.getValue()));

        if (slider == maxSlider)
            minSlider.setMaximum(Math.min(16384 - 256, maxSlider.getValue()));
    }

    public void applyCorrection() {
        if (cine != null) {
            // parametrem funkcji setCorrection jest funkcją typu double doCorrection(double value)
            // gdzie parametr value, z zakresu 0 - 1, jest mapowany na wartosc takze z zakresu 0 - 1.
            // poniżej, jako przyklad, korekcja gamma.
            double gammaFactor = 1000.0 / gammaSlider.getValue();
            double minFactor = minSlider.getValue() / 16384.;
            double maxFactor =  maxSlider.getValue() / 16384.;

            cine.setCorrection(a -> {
                double tmp;
                if (a < minFactor) {
                    tmp = 0;
                }else if ( a > maxFactor) {
                    tmp =  1;
                } else {
                    tmp = (a - minFactor) / maxFactor;
                }

                return Math.pow(tmp, gammaFactor);
            });
            showFrame();
        }
    }

    public void playStop() {
        playStop = !playStop;
        if (playStop) {
            play.setIcon(new ImageIcon(pausePath));
            tm.start();
        } else {
            play.setIcon(new ImageIcon(playPath));
            tm.stop();
            cine.setBuffPos();
        }
    }

    public void showPreviousFrame() {
        playStop = false;
        cine.prevFrame();
        showFrame();
    }

    public void showNextFrame() {
        playStop = false;
        cine.nextFrame();
        showFrame();
    }

    public void openFile() {
        fileChooser.showOpenDialog(window);
        File file = fileChooser.getSelectedFile();

        if (file != null) {
            tm.stop();
            try {
                cine = new CineMovie(file);
                applyFilter();
                applyReflection();
                applyCorrection();
                showFrame();
                tm.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
