package film.read;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.awt.image.BufferedImage;

import static java.lang.Integer.min;

class CineStream extends FileInputStream {
    private File path;    // nazwa pliku

    public CineStream(File file) throws FileNotFoundException {
        super(file);
        path = file;
    }

    public short getByte() throws IOException { return (short) (read()); }
    public int getWord() throws IOException { return (read() + 256 * read()); }
    public long getLong() throws IOException { return getWord() + 65536 * getWord(); }
    public long get64Bit() throws IOException { return getLong() + 65536 * 65536 * getLong(); }
    public void setPosition(long position) throws IOException { getChannel().position(position); }
}

class CineFile extends CineStream {
    // klasy pomocnicze - nie beda stosowane na "zewnatrz"
    public class CineFileHeader {
        int type = 0x4943;    // zawsze ma tutaj być "CI" czyli wlasnie 0x4943
        int headerSize;
        int compression;
        int version;
        long firstMovieImage;
        long totalImageCount;
        long firstImageNo;
        long imageCount;
        long offImageHeader;
        long offSetup;
        long offImageOffsets;
        char[] triggerTime = new char[8];
        double triggertimeF;
        final double x = 1000 / Math.pow(2, 32);    //1000 bo w ms zamiast sekund
        Date date;

        public CineFileHeader() {
            try {
                type = getWord();
                headerSize = getWord();
                compression = getWord();
                version = getWord();
                firstMovieImage = getLong();
                totalImageCount = getLong();
                firstImageNo = getLong();
                imageCount = getLong();
                offImageHeader = getLong();
                offSetup = getLong();
                offImageOffsets = getLong();
                getLong();// pomijanie pola fractions
                date = new Date(getLong() * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public long getImageCount() { return imageCount; }
        public long getOffImageOffsets() { return offImageOffsets; }

    }
    public class BitMapInfoHeader {
        long biSize;
        long biWidth;
        long biHeight;
        int biPlanes;
        int biBitCount;
        long biCompression;
        long biSizeImage;
        long biXPelsPerMeter;
        long biYPelsPerMeter;
        long biClrUsed;
        long biClrImportant;

        public BitMapInfoHeader() {
            try {
                biSize = getLong();
                biWidth = getLong();
                biHeight = getLong();
                biPlanes = getWord();
                biBitCount = getWord();
                biCompression = getLong();
                biSizeImage = getLong();
                biXPelsPerMeter = getLong();
                biYPelsPerMeter = getLong();
                biClrUsed = getLong();
                biClrImportant = getLong();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public long getBiWidth() { return biWidth; }
        public long getBiHeight() { return biHeight; }

    }
    public class CameraSetup {
        int frameRate16;
        int shutter16;
        int postTrigger16;
        int frameDelay16;
        int aspectRatio;
        int contrast16;
        int bright16;
        short rotate16;
        short timeAnnotation;
        short trigCine;
        short trigFrame;
        short shutterOn;
        short decriptionOld;
        int mark;
        int length;
        int binning;
        int sigOption;
        int binChannels;
        short samplesPerImage;

        public CameraSetup() {
            try {
                frameRate16 = getWord();
                shutter16 = getWord();
                postTrigger16 = getWord();
                frameDelay16 = getWord();
                aspectRatio = getWord();
                contrast16 = getWord();
                bright16 = getWord();
                rotate16 = getByte();
                timeAnnotation = getByte();
                trigCine = getByte();
                trigFrame = getByte();
                shutterOn = getByte();
                decriptionOld = getByte();
                mark = getWord();
                length = getWord();
                binning = getWord();
                sigOption = getWord();
                binChannels = getWord();
                samplesPerImage = getByte();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    CineFileHeader cineFileHeader;
    BitMapInfoHeader bitMapInfoHeader;
    CameraSetup cameraSetup;
    Map<Integer, Long> idAnnotation = new HashMap<>();
    List<Long> positions = new ArrayList<>();

    protected short frameWidth;   // szerokosc klatki
    protected short frameHeight;  // wysokosc klatki
    protected int frameCount;     // liczba klatek w pliku
    protected Long frameDataOffset;   // pozycja 1 klatki w filmie
    protected int frameDataLength;   // ilosc bajtow w pliku opisujaca pojedyncza klatke
    String[] infoHeader = new String[14];

    public CineFile(File file) throws IOException {
        super(file);

        // odczyt informacji o pliku i bitmapie
        cineFileHeader = new CineFileHeader();
        bitMapInfoHeader = new BitMapInfoHeader();
        cameraSetup = new CameraSetup();

        //informacje do wyświetlenia
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
        infoHeader[0] = String.valueOf(cineFileHeader.firstMovieImage);
        infoHeader[1] = String.valueOf(cineFileHeader.totalImageCount);
        infoHeader[2] = String.valueOf(cineFileHeader.firstImageNo);
        infoHeader[3] = String.valueOf(cineFileHeader.imageCount);
        infoHeader[4] = DATE_FORMAT.format(cineFileHeader.date);
        infoHeader[5] = String.valueOf(bitMapInfoHeader.biWidth);
        infoHeader[6] = String.valueOf(bitMapInfoHeader.biHeight);
        infoHeader[7] = String.valueOf(cameraSetup.frameRate16);
        infoHeader[8] = String.valueOf(cameraSetup.aspectRatio);
        infoHeader[9] = String.valueOf(cameraSetup.shutter16);
        infoHeader[10] = String.valueOf(cameraSetup.postTrigger16);


        // zapamietanie najwazniejszych informacji do szybkiego dostepu
        frameWidth = (short) bitMapInfoHeader.getBiWidth();
        frameHeight = (short) bitMapInfoHeader.getBiHeight();
        frameCount = (int) cineFileHeader.getImageCount();
//        frameDataOffset = cineFileHeader.getOffImageOffsets();

        // obliczenie dlugosci pakiedu danych opisujacego pojedycza klatke
        long position = cineFileHeader.getOffImageOffsets();

        for (int i = 0; i < frameCount; i++) {
            setPosition(position);
            frameDataOffset = get64Bit();
            positions.add(frameDataOffset);
            setPosition(frameDataOffset);
            idAnnotation.put(i, getLong());
            position += 8;
        }
        int tmp = 0;
        for (Long x : idAnnotation.values()) {
            if (tmp < x) {
                long value = x;
                tmp = (int) value;
            }
        }
//        setPosition(cineFileHeader.getOffImageOffsets());
//        frameDataOffset = get64Bit();
//        frameDataLength = (int) (get64Bit() - frameDataOffset);   // nie zmieniac bo dane musza byc czytane w odpowiedniej kolejnosci!!!

            frameDataLength = frameWidth * frameHeight * 2;
    }


    // odczyt pojedynczej klatki do bufora frame
    public void loadFrame(int id, byte[] buffer) {
        try {
            int frameId = (id + frameCount) % frameCount;
            long skipAnnotation = idAnnotation.get(frameId);
            setPosition(positions.get(frameId) + skipAnnotation);
            read(buffer, 0, frameDataLength);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

}

public class CineMovie extends CineFile {

    public class Frame {
        private byte[] data = new byte[frameDataLength];  // "surowe" dane - każdy piksel o wartości od 0 do 16383 (14 bitów)

        private short pixel(int idx) {
            return (short) ((data[idx] & 0xff) | ((data[idx + 1] & 0xff) << 8));
        }

        public BufferedImage getFrameImage() {
            // indeksy w tablicy do przetwarzanego piksela
            int cIdx = 0;    //+8 : uwzglednienie Annotation
            int pIdx = cIdx - 2 * frameWidth; // wczesniejsza linia
            int nIdx = cIdx + 2 * frameWidth; // nastepna linia

            for (int y = 0; y < frameHeight; y++) {
                for (int x = 0; x < frameWidth; x++) {

                    // aplikacja filtra
                    short pix = pixel(cIdx);
                    if (filter != null && x > 0 && x < frameWidth - 1 && y > 0 && y < frameHeight - 1) {
                        // nakladanie filtra
                        pix = (short) (
                                filter[0] * pixel(pIdx - 2) + filter[1] * pixel(pIdx) + filter[2] * pixel(pIdx + 2) +
                                        filter[3] * pixel(cIdx - 2) + filter[4] * pixel(cIdx) + filter[5] * pixel(cIdx + 2) +
                                        filter[6] * pixel(nIdx - 2) + filter[7] * pixel(nIdx) + filter[8] * pixel(nIdx + 2)
                        );

                        // ograniczenie wartosci piksela do zadanego przedzialu
                        if (pix < 0) {
                            pix = 0;
                        } else if (pix > 16383) {
                            pix = 16383;
                        }
                    }

                    // korekcja gamma
                    int c = gamma[pix];
                    int rgb = (c << 16) + (c << 8) + c;

                    // obsługa odbic
                    switch (reflection) {
                        case 0: // odbicie wzgledem osi X
                            frameImage.setRGB(frameWidth - 1 - x, y, rgb);
                            break;

                        case 1: // odbicie wzgledem osi Y
                            frameImage.setRGB(x, frameHeight - 1 - y, rgb);
                            break;

                        case 2: // odbiie wzgledem osi X i Y (obrot o 180 stopni)
                            frameImage.setRGB(frameWidth - 1 - x, frameHeight - 1 - y, rgb);
                            break;

                        default:
                            frameImage.setRGB(x, y, rgb);
                            break;
                    }
                    cIdx += 2;
                    pIdx += 2;
                    nIdx += 2;
                }
            }

            return frameImage;
        }
    }

    // obsluga bufora cyklicznego
    protected static final short BUFF_MAX_CAPACITY = 40;
    protected static final short BUFF_MIN_SIZE = BUFF_MAX_CAPACITY / 10; // minimalny bufor to 10% calosci

    private Frame[] frames;
    private int buffCapacity;
    private int buffPos;
    private int buffEnd;
    private int frameId;
    private int id;
    private int imageNumber;
    private boolean isPrevious = false;

    private byte[] filter;
    private int[] gamma = new int[16384];
    private int reflection = -1;

    private BufferedImage frameImage;

    public int getImageNumber() {
        return imageNumber;
    }

    public void setBuffPos() {
        this.buffPos = buffPos - 1;
    }

    public String[] getInfoFromHeader() {
        return infoHeader;

    }

    public CineMovie(File file) throws IOException {
        super(file);
        // tworzenie bufora cyklicznego
        buffCapacity = min(frameCount, BUFF_MAX_CAPACITY);
        frames = new Frame[buffCapacity];
        id = buffCapacity;

        for (int i = 0; i < buffCapacity; i++) {
            frames[i] = new Frame();
        }

        // odczyt klatek do bufora
        buffPos = buffCapacity / 2;
        for (int i = 0; i < buffCapacity; i++) {
            loadFrame(i - buffPos, frames[i].data);
        }
        buffEnd = buffCapacity - 1;
        imageNumber = buffPos - 1;
        // utworzenie bufora dla przetwarzanej klatki
        frameImage = new BufferedImage(frameWidth, frameHeight, BufferedImage.TYPE_INT_RGB);

        // prekalkulacja tablicy dla korekcji gamma (gamma = 1)
        for (int idx = 0; idx < 16384; idx++) {
            gamma[idx] = idx >> 6; // dzielenie przez 64
        }
    }

    public Frame getFrame() {
        this.frameId = buffPos;
        if (isPrevious) {
            --imageNumber;
        } else {
            ++imageNumber;
        }
        imageNumber = ((imageNumber + frameCount) % (frameCount));

        return frames[buffPos];
    }

    public void prevFrame() {
        buffPos = (buffPos - 1 + buffCapacity) % buffCapacity;
        isPrevious = true;
        // jesli bufor nie zawiera calego filmu, i sie "kurczy, to nalezy zaladowac wczesniejsze klatki
//        if (buffCapacity < frameCount) {
//            int buffSize = (buffCapacity + buffPos - buffEnd) % buffCapacity;
//            if (buffSize <= BUFF_MIN_SIZE)
//                for (int i = 0; i < BUFF_MIN_SIZE; i++) {
//                    id = (id + frameCount) % frameCount;
//                    buffEnd = (buffEnd + 1 + buffCapacity) % buffCapacity;
//                    loadFrame(--id, frames[buffEnd].data);
//                }
//        }
    }

    public void nextFrame() {
        buffPos = (buffPos + 1 + buffCapacity) % buffCapacity;
        isPrevious = false;
        // jesli bufor nie zawiera calego filmu, i sie "kurczy", to nalezy zaladowac nastepne klatki
        if (buffCapacity < frameCount) {
            int buffSize = (buffCapacity - buffPos + buffEnd) % buffCapacity;
            if (buffSize <= BUFF_MIN_SIZE)
                for (int i = 0; i < BUFF_MIN_SIZE; i++) {
                    id = (id + frameCount) % frameCount;
                    buffEnd = (buffEnd + 1 + buffCapacity) % buffCapacity;
                    loadFrame(id++, frames[buffEnd].data);
                }
        }
    }

    public void setFilter(byte[] filter) {
        assert filter.length == 9;
        this.filter = filter;
    }
    public void setReflection(int reflection) {
        this.reflection = reflection;
    }

    public interface ICorrection {
        double calculate(double value);
    }

    public void     setCorrection(ICorrection correction) {
        for (int idx = 0; idx < 16384; idx++) {
            gamma[idx] = (int) (correction.calculate(idx / 16384.0) * 255);
        }
    }
}
